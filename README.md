# Descarga CFDI del SAT

## Script para descargar de forma automática documentos electrónicos del SAT.

### Mira el [wiki](https://gitlab.com/mauriciobaeza/cfdi-descarga/wikis/home) para los detalles de uso.

### Ayudanos a ayudar a otros, conoce [nuestras actividades](http://universolibre.org/hacemos/).
